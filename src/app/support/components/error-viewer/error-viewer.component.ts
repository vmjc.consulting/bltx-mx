import { Component, Input, OnInit } from '@angular/core';
import { FormGroup, AbstractControl } from '@angular/forms';

@Component({
	selector: 'error-viewer',
	templateUrl: './error-viewer.component.html',
	styleUrls: ['./error-viewer.component.css']
})
export class ErrorViewerComponent implements OnInit {

	@Input()
	form: FormGroup;

	@Input()
	public property: string;

	@Input()
	public config: any;

	private control: AbstractControl;

	constructor() { };

	ngOnInit() {
		this.control = this.form.get(this.property);
	};

	get hasName(): boolean {
		return (this.config.name != null && this.config.name != undefined && this.config.name != '');
	};

	get hasMessage(): boolean {
		return (this.config.message != null && this.config.message != undefined && this.config.message != '');
	};

	get hasCompareName(): boolean {
		return (this.config.compareName != null && this.config.compareName != undefined && this.config.compareName != '');
	};

	get texts() {

		return {
			required: (this.hasName ? 'El campo ' + this.config.name + ' es requerido.' : 'El campo es requerido.'),
			minLength: (this.hasName ? 'El campo ' + this.config.name + ' debe tener ' + this.minLength + ' caracteres como mínimo.' : 'El campo debe tener ' + this.minLength + ' caracteres como mínimo.'),
			maxLength: (this.hasName ? 'El campo ' + this.config.name + ' debe contener ' + this.maxLength + ' caracteres como máximo.' : 'El campo debe contener ' + this.maxLength + ' caracteres como máximo.'),
			pattern: (this.hasName ? 'El campo ' + this.config.name + ' no tiene el formato correcto.' : 'El campo no tiene el formato correcto.'),
			parseError: (this.hasName ? 'El campo ' + this.config.name + ' no pudo ser convertido.' : 'El campo no pudo ser convertido.'),
			selectNotEquals: (this.hasName ? 'El campo ' + this.config.name + ' es requerido. ' : 'El campo es requerido.'),
			email: (this.hasName ? 'El campo ' + this.config.name + ' no tiene el formato de un correo válido.' : 'El formato de correo no es válido.'),
			compareValidate: (this.hasMessage ? this.config.message : (this.hasName && this.hasCompareName ? 'El campo ' + this.config.compareName + ' y el campo ' + this.config.name + ' no coinciden. ' : 'Los campos no coinciden.')),
			patternInvalid: (this.hasName ? 'El campo ' + this.config.name + ' no tiene el formato correcto.' : 'El formato no es correcto.')
		};
	};

	get minLength(): number {
		let target: number = 0;
		if (this.control) {
			if (this.control.errors) {
				if (this.control.errors.minlength) {
					if (this.control.errors.minlength.requiredLength) {
						target = this.control.errors.minlength.requiredLength;
					}
				}
			}
		}
		return target;
	};

	get maxLength(): number {
		let target: number = 0;
		if (this.control) {
			if (this.control.errors) {
				if (this.control.errors.maxlength) {
					if (this.control.errors.maxlength.requiredLength) {
						target = this.control.errors.maxlength.requiredLength;
					}
				}
			}
		}
		return target;
	};

	get selectNotEquals(): string {
		let target: string = "0";
		if (this.control) {
			if (this.control.errors) {
				if (this.control.errors.selectNotEquals) {
					if (this.control.errors.selectNotEquals.invalidValue !== null && this.control.errors.selectNotEquals.invalidValue !== undefined) {
						target = `'${this.control.errors.selectNotEquals.invalidValue}'`;
					}
				}
			}
		}
		return target;
	};

	debug() {
		console.info('this.form, this.property, this.config', this.form, this.property, this.config);
		console.info('this.form.get(this.property)', this.form.get(this.property));
	};

}
