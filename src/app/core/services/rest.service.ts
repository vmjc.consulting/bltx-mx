import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

import { HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { environment } from '@environments/environment';

import { GenericRestService } from '@app-support-services/';

@Injectable()
export class RestService {

	private actionUrl: string;

	constructor(private http: GenericRestService) {
		this.actionUrl = `${environment.urlApi}?`;
	}

	get(base: string, symbols: Array<string>): Observable<any> {
		let internalSymbols: string = symbols.join(',');
		return this.http.get<any>(`${this.actionUrl}base=${base}&symbols=${internalSymbols}`)
			.catch(this.handleError);
	}

	// post(url: string, entity: any = {}, options?: any): Observable<any> {
	// 	return this.http.post<any>(`${this.actionUrl}/${url}`, entity, options).catch(this.handleError);
	// }

	// put(url: string, entity: any): Observable<any> {
	// 	return this.http.put<any>(`${this.actionUrl}/${url}`, entity).catch(this.handleError);
	// }

	// delete(url: string): Observable<any> {
	// 	return this.http.delete<any>(`${this.actionUrl}/${url}`).catch(this.handleError);
	// }

	private handleError(error: HttpResponse<any>) {
		return Observable.throw(error || 'Server error');
	}

}    