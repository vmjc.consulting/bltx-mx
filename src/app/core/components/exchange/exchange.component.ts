import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CurrencyPipe } from '@angular/common';
import { RestService } from '@app/core/services/rest.service';

import { Subject } from 'rxjs';
import 'rxjs/Rx';

@Component({
  templateUrl: 'exchange.component.html'
})
export class ExchangeComponent implements OnInit, OnDestroy {
  form: FormGroup;
  public isLoading: boolean = false;
  private destroy$: Subject<void> = new Subject<void>();
  private base: string = 'USD';
  private symbols: Array<string> = ['EUR'];
  private currency: CurrencyPipe = new CurrencyPipe('en-US');
  private format: string = '1.1-4';
  public currencyMaskOptions = { prefix: '$', thousands: ',', decimal: '.', precision: 4 };
  private rates: Array<any> = [];
  private intervalTime: number = 10 * 60 * 1000;

  constructor(
    private readonly fb: FormBuilder,
    private readonly restService: RestService) {
    this.createForm();
  };

  ngOnInit() {
    this.getRates();
    this.autoRefreshRates();
  };

  private createForm(): void {
    this.form = this.fb.group({
      Amount: [ '', Validators.compose([ Validators.required ]) ],
      ResultAmount: ['']
    });
  };

  getRates() {
    this.restService.get(this.base, this.symbols)
      .takeUntil(this.destroy$)
      .subscribe(response => {
        this.isLoading = false;
        if (response.rates) {
          this.rates = response.rates;
        }
      }, error => {
        this.isLoading = false;
      });
  };

  autoRefreshRates() {
    setInterval(() => this.getRates(), this.intervalTime);
  };

  calculate() {
    if (this.form.valid) {
      let amount = this.form.get('Amount').value;
      this.form.get('ResultAmount').reset();
      amount = parseFloat(amount);
      if (!isNaN(amount)) {
        let rate = this.rates[this.symbols[0]];
        rate = parseFloat(rate);
        if (!isNaN(rate)) {
          let value = (amount * rate).toString();
          value = this.currency.transform(value, this.symbols[0], true, this.format);
          this.form.patchValue({ 'ResultAmount': value });
          this.form.get('ResultAmount').markAsDirty();
        }
      }
    }
  };

  ngOnDestroy() {
		this.destroy$.next();
    this.destroy$.complete();
	};
}